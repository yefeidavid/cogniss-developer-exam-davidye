const fs = require("fs");

const rawData = fs.readFileSync("input2.json");

let names = JSON.parse(rawData).names;

names = names.map((name) => {
  let newName = name.toLowerCase().split("").reverse("").join("");
  for (let i = 0; i < 5; i++) {
    let randomChar = String.fromCharCode(97 + Math.floor(Math.random() * 26));
    newName = newName.slice(0, 1).toUpperCase() + newName.slice(1) + randomChar;
  }
  return newName + "@gmail.com";
});

const result = {emails:[]}

for (let i = 0; i < names.length; i++) {
    result.emails.push(names[i]);   
}

const emails = JSON.stringify(result);

fs.writeFile("output2.json", emails, (err)=>{
    if (err) {
        throw err;
    }
});
